@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

                    @if (session('status'))
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                </div>
            </div>
        </div>
                    @endif
    </div>
</div>
<div>             
 <div class="container" style="position: relative;height: auto;min-height: 100vh;    padding-bottom: 44px;">
            <div class="namepic">
                <div>
                    <p class="display-4 mt-2 name-surname" style="font-size: 30px;"><?php echo Auth::user()->name.' '.Auth::user()->surname ?></p><br>
                </div>
                <div class="profpic" style="background-image: url(<?php echo json_decode(Auth::user()->user_pics()->where('id', Auth::user()->profpic_id)->get())[0]->pic; ?>);">
                    <div class="profpic-hover">
                        <button class="btn btn-neutral galery" type="button" data-toggle="modal" data-target="#myModal">Choose photo</button>
                    </div>
                </div>
                <div class="friendlist" ></div>
            </div>
        <div class="post-container">
            <div class="posts">
                <div class="new-post">
                    <h2>Make a new post</h2>
                    <form action="{{url('/new_post')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="text" id='text' name="text" autocomplete="off" style="width: 500px;height: 160px;padding-bottom: 90px;"><br>
                        <div class="file">
                            <input type="file" size="90" name="file"    class="file_pick">
                        </div>
                        <button class="btn btn-primary" value="new_post" style="float: right;">Post</button>
                    </form><br><br><br>
                </div>
                <div class="all-posts"></div>
            </div>
        </div>
        </div>
    </div>
    <!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Choose Photo for your profile</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <div class="container">
        <div class="row galery-row" style="display: flex;justify-content: space-between;">
            <div>
                <form action="{{url('/upload_pic')}}" method="post"  id="gal-form" enctype="multipart/form-data">
                    @csrf
                    <label for="upload-photo" class="gal-upload">+</label>
                    <input type="file" name="galery-photo" id="upload-photo" style="display: none;">
                </form>
            </div>
            
        </div>
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
@endsection
