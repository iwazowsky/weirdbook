<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_pic extends Model
{
    protected $fillable = [
        'user_id','pic'
    ];
    public function post_pics()
    {
        return $this->belongsTo('App\User');
    }
}
