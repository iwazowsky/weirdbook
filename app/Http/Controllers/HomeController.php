<?php

namespace App\Http\Controllers;
use Auth;
use App\User_pic;
use App\User_post;
use DB;
use Redirect;
use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function get_user_pics()
    {
        $id = Auth::id();   
        echo json_encode(Auth::user()->user_pics()->where('user_id', $id)->get());

    }
    public function set_profpic(request $data)
    {
        $user_id = Auth::id();
        $pic_id = $data['id'];
        User::whereId($user_id)->update(['profpic_id'=>$pic_id]);
    }
    public function get_user_posts()
    {
        $user_id = Auth::id();
       echo User_post::with('user_posts.id','user_posts.text','user_pics.pic')->where('user_id', $user_id)->get();
    }
    public function new_post(request $data)
    {   

        $user_id = Auth::id();
        if(!empty($data['text']) && isset($data['text']))
            $text = $data['text'];
        else $text = '';
        if(!empty($data['file']) && isset($data['file'])){
            $pic = $data['file'];
            $tmp = $pic->path();
            $name = $pic->getClientOriginalName();
            $destination = "images/".uniqid().$name;
            move_uploaded_file($tmp, $destination);
            $pic = User_pic::create([
            'user_id'=>$user_id,
            'pic'=>$destination
            ]);
            $pic = $pic->id;
        }
        else $pic = null;
        if((!empty($data['file']) && isset($data['file'])) || (!empty($data['text']) && isset($data['text'])))
        {
            $post = User_post::Create([
                'user_id' => $user_id,
                'text' => $text,
                'pic_id'=>$pic
            ]);
        }
        return redirect('home');
    }
    public function upload_pic(request $data)
    {   
        $user_id = Auth::id();
        $tmp = $data['galery-photo']->path();
        $name = $data['galery-photo']->getClientOriginalName();
        $destination = "images/".uniqid().$name;
        move_uploaded_file($tmp, $destination);
        $pic = User_pic::create([
            'user_id'=>$user_id,
            'pic'=>$destination
        ]);
        $pic_id = $pic->id;
        User::whereId($user_id)->update(['profpic_id'=>$pic_id]);
        return redirect('home');
    }
}
