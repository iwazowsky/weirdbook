<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','surname','username','email', 'password','profpic_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function user_posts()
    {
        return $this->hasMany('App\User_post','id','user_id');
    }
    public function user_pics()
    {
        return $this->hasMany('App\User_pic');
    }
    protected $hidden = [
        'password', 'remember_token',
    ];
}
