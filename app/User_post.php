<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class User_post extends Model
{
     use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','text','pic_id'
    ];
    public function post_pics()
    {
        return $this->hasOne('App\User_pic','pic_id','id');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
