<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/get_user_pics', 'HomeController@get_user_pics');
Route::post('/upload_pic', 'HomeController@upload_pic');
Route::post('/new_post', 'HomeController@new_post');
Route::post('/set_profpic', 'HomeController@set_profpic');
Route::post('/get_user_posts', 'HomeController@get_user_posts');

