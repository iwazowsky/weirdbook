$(document).ready(()=>{
	$.ajaxSetup({
  		headers: {
    		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  		}
	});
	$.ajax({
		type:'post',
		url:'/get_user_posts',
		dataType: 'json',
		success: r =>{
			r = r.reverse()	
			r.forEach(item => {
				if(item)
				$('.all-posts').append(`<div class="status"></div>`)
				if(item.text != '') {
					$(`.status`).last().append(`<p  align='center' class="status-p">${item.text}</p>`)
				}
					if(item.pic != '') $(`.status`).last().append(`<div class="status-img" style='background: url("${item.pic}"); background-size:cover; height:300px; width:533px;'></div>`)
					// $(`.status`).last().append(` <div class="action-bar" style='border-top:1px solid lightgray;'>
					// 	<div class="like" style="cursor:pointer;display: inline-block;margin-left:15px;" data-id="${item.post_id}">
					// 		<i class='${item.liked} fa-heart fa-2x'></i><span style="font-size:30px">${item.like_num}</span>
					// 	</div>
					// 	</div> `)
				
			})
		}

	})
	$.ajax({
		type:'post',
		url:'/get_user_pics',
		dataType: 'json',
		success: r =>{
			r.forEach(item => {
				$('.galery-row').append(`<div class="galery-item" data-id="${item.id}" style='background:url("${item.pic}") no-repeat;  background-position: center;  background-size:cover; cursor: pointer; width: 100px; height: 100px' data-dismiss="modal"></div>`)
			})
		}
	})

})
// $('.search').on('input',function(){
// 	let text = $(this).val()
// 	if(!text.trim().length){
// 		$('.search-results').empty()
// 		return
// 	}
// 		$.ajax({
// 			type: 'post',
// 			url: 'server.php',
// 			data: {action:'search_users',name:text},
// 			dataType: 'json',
// 			success: r => {
// 					$('.search-results').empty()
// 				r.forEach(item => {
// 					$('.search-results').append(`<div class="form-control user-result"><a href='other_user.php?id=${item.id}'>${item.name}  ${item.surname}</a></div>`)
// 				})
// 			}
// 		})
// })
$(document).on('change','#upload-photo',()=>{
    $('#gal-form').submit()
 })

$(document).on('click','.galery-item',function(){
	let id = $(this).data('id')
	let src = $(this).css('background-image')
       	src = src.replace('url(','').replace(')','').replace(/\"/gi, "");	
	$.ajax({
		type:'post',
		url:'/set_profpic',
		data:{id:id},
		success: r => {
			$('.profpic').css('background-image',`url(${src})`)
		}
	})
})
